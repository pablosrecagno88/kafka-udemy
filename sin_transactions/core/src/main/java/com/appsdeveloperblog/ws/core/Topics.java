package com.appsdeveloperblog.ws.core;

public class Topics {
    public static final String PRODUCT_CREATED_EVENT_TOPIC = "product-created-event-topic";
    public static final String TEST_TOPIC = "test.topic";
}
