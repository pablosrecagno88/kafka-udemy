package com.appsdeveloperblog.ws.emailnotification.handler;

import com.appsdeveloperblog.ws.core.ProductCreatedEvent;
import com.appsdeveloperblog.ws.core.Topics;
import com.appsdeveloperblog.ws.emailnotification.error.NotRetryableException;
import com.appsdeveloperblog.ws.emailnotification.error.RetryableException;
import com.appsdeveloperblog.ws.emailnotification.io.ProcessedEventEntity;
import com.appsdeveloperblog.ws.emailnotification.io.ProcessedEventRepository;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Component
@KafkaListener(topics = Topics.PRODUCT_CREATED_EVENT_TOPIC)
@Slf4j
public class ProductCreatedEventHandler {

    private final RestTemplate restTemplate;
    private final ProcessedEventRepository processedEventRepository;

    public ProductCreatedEventHandler(RestTemplate restTemplate, ProcessedEventRepository processedEventRepository) {
        this.restTemplate = restTemplate;
        this.processedEventRepository = processedEventRepository;
    }

    @Transactional
    @KafkaHandler
    public void handle(
            @Payload ProductCreatedEvent productCreatedEvent, @Header("messageId") String messageId, @Header(KafkaHeaders.RECEIVED_KEY) String messageKey) {
        log.info("ProductCreatedEvent received with messageId {}, productId {} and product title {}",
                messageId,productCreatedEvent.getProductId(), productCreatedEvent.getTitle());
        String url = "http://localhost:8082/response/200";

        ProcessedEventEntity processedEventFound = processedEventRepository.findByMessageId(messageId);

        if (Objects.nonNull(processedEventFound)){
            log.error("Found a product with messageId already processed {}",processedEventFound.getMessageId());
            return;
        }

        try {
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null, String.class);

            if (HttpStatus.OK.value() == response.getStatusCode().value()) {
                log.info("Received response from a remote service: {}", response.getBody());
            }
        } catch (ResourceAccessException resourceAccessException) {
            log.error(resourceAccessException.getMessage());
            throw new RetryableException(resourceAccessException);
        } catch (HttpServerErrorException httpServerErrorException) {
            log.error(httpServerErrorException.getMessage());
            throw new NotRetryableException(httpServerErrorException);
        } catch (Exception exception) {
            log.error(exception.getMessage());
            throw new NotRetryableException(exception);
        }

        try{
            processedEventRepository.save(buildProcessedEvent(messageId, productCreatedEvent.getProductId()));
        }catch (DataIntegrityViolationException exception){
            throw new NotRetryableException(exception);
        }
    }

    private ProcessedEventEntity buildProcessedEvent(String messageId, String productId){
        return new ProcessedEventEntity(messageId,productId);
    }
}
