package com.appsdeveloperblog.ws.products.service;

import com.appsdeveloperblog.ws.core.ProductCreatedEvent;
import com.appsdeveloperblog.ws.core.Topics;
import com.appsdeveloperblog.ws.products.rest.CreateProductRestModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

    //private static final String MESSAGE_ID_REPEATED="MENSAJE_REPETIDO";

    KafkaTemplate<String, ProductCreatedEvent> kafkaTemplate;

    public ProductServiceImpl(KafkaTemplate<String, ProductCreatedEvent> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public String createProductSync(CreateProductRestModel productRestModel) throws Exception {//Forma síncrona de hacerlo
        log.info("[ProductServiceImpl::createProductSync] request input {}", productRestModel);

        String productId = buildProductId();

        // TODO persist product details into DB before publishing an event

        ProductCreatedEvent productCreatedEvent = buildProductEvent(productId, productRestModel);

        log.info("[ProductServiceImpl::createProductSync] before send event {} with id {}", productCreatedEvent, productId);

        ProducerRecord<String, ProductCreatedEvent> producerRecord = buildProducerRecord(productId, productCreatedEvent);
        SendResult<String, ProductCreatedEvent> result = kafkaTemplate.send(producerRecord).get();

        log.info("[ProductServiceImpl::createProductSync] return productId {}", productId);
        log.info("[ProductServiceImpl::createProductSync] return topic name {}", result.getRecordMetadata().topic());
        log.info("[ProductServiceImpl::createProductSync] return partition name {}", result.getRecordMetadata().partition());
        log.info("[ProductServiceImpl::createProductSync] return offset id {}", result.getRecordMetadata().offset());

        return productId;
    }

    @Override
    public String createProductAsync(CreateProductRestModel productRestModel) {//Forma asíncrona de hacerlo
        log.info("[ProductServiceImpl::createProductAsync] request input {}", productRestModel);

        String productId = buildProductId();

        // TODO persist product details into DB before publishing an event

        ProductCreatedEvent productCreatedEvent = buildProductEvent(productId, productRestModel);
        CompletableFuture<SendResult<String, ProductCreatedEvent>> future = kafkaTemplate.send(Topics.PRODUCT_CREATED_EVENT_TOPIC, productId, productCreatedEvent);

        future.whenComplete((result, exception) -> {
            if (Objects.nonNull(exception)) {
                log.error("[ProductServiceImpl::createProductAsync] Error sending event {} to kafka {}", productCreatedEvent, exception.getMessage());
            } else {
                log.info("[ProductServiceImpl::createProductAsync] Event sent succesfully {}", result.getRecordMetadata());
            }
        });

        future.join();
        //Hace que deje de ser asincrono y sea sincrono. Es decir, bloquea el hilo esperando la respuesta del ACK de Kafka

        log.info("[ProductServiceImpl::createProductAsync] return productId {}", productId);

        return productId;
    }

    private ProductCreatedEvent buildProductEvent(String productId, CreateProductRestModel productRestModel) {
        return new ProductCreatedEvent(
                productId,
                productRestModel.getTitle(),
                productRestModel.getPrice(),
                productRestModel.getQuantity());
    }

    private String buildProductId() {
        return UUID.randomUUID().toString();
    }

    private ProducerRecord<String,ProductCreatedEvent> buildProducerRecord(String messageKey,ProductCreatedEvent productCreatedEvent){
        ProducerRecord<String, ProductCreatedEvent> producerRecord =
                new ProducerRecord<>(Topics.PRODUCT_CREATED_EVENT_TOPIC, messageKey, productCreatedEvent);

        byte[] messageId = buildProductId().getBytes();
        producerRecord.headers().add("messageId", messageId);

        return producerRecord;
    }
}
