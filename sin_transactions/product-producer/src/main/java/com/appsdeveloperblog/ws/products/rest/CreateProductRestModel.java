package com.appsdeveloperblog.ws.products.rest;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
public class CreateProductRestModel {
    private String title;
    private BigDecimal price;
    private int quantity;
}
