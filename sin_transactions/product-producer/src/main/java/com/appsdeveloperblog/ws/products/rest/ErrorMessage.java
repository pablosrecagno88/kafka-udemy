package com.appsdeveloperblog.ws.products.rest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class ErrorMessage {
    private Date timestamp;
    private String message;
    private String details;
}
