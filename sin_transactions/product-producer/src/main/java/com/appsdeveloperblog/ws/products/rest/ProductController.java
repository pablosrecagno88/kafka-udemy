package com.appsdeveloperblog.ws.products.rest;

import com.appsdeveloperblog.ws.products.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/products")
@Slf4j
public class ProductController {

    ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping
    public ResponseEntity<Object> createProductSync(@RequestBody CreateProductRestModel requestInput) {
        log.info("[ProductController::createProductSync] input data {}", requestInput);

        String productId = null;

        try{
            productId = productService.createProductSync(requestInput);
        }catch (Exception exception){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(buildErrorMessage(exception,"/products"));
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(productId);
    }

    private ErrorMessage buildErrorMessage(Exception exception, String url){
        return ErrorMessage.builder()
                .timestamp(new Date())
                .message(exception.getMessage())
                .details(url)
                .build();
    }
}
