package com.appsdeveloperblog.ws.products.service;

import com.appsdeveloperblog.ws.products.rest.CreateProductRestModel;

import java.util.concurrent.ExecutionException;

public interface ProductService {
    String createProductAsync(CreateProductRestModel productRestModel);
    String createProductSync(CreateProductRestModel productRestModel) throws Exception;
}
